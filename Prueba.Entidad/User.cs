﻿namespace Prueba.Entity
{
    using System.ComponentModel.DataAnnotations;
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// User Type
    /// </summary>
    [Table("User")]
    public class User
    {
        /// <summary>
        /// User ID
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// User Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Last Name User
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Address User
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Date create user
        /// </summary>
        public string CreateDate { get; set; }

        /// <summary>
        /// Date Update user
        /// </summary>
        public string UpdateDate { get; set; }
    }
}
