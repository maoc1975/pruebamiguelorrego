﻿using System.Web.Http;
using Autofac;
using Prueba.Management.Interfaces;


namespace Prueba.Api.Controllers
{
    public class UserDeleteController : ApiController
    {
        public bool Get(int id)
        {
            var contenedorIoC = ConfigurationIoC.GeneradorContainer();
            return contenedorIoC.Resolve<IUser>().Delete(id);
        }
    }
}
