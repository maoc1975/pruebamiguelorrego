﻿using System.Collections.Generic;
using System.Web.Http;
using Prueba.Entity;
using Prueba.Management.Interfaces;
using Autofac;

namespace Prueba.Api.Controllers
{
    public class UserListController : ApiController
    {
        public IEnumerable<User> Get()
        {
            var contenedorIoC = ConfigurationIoC.GeneradorContainer();
            return contenedorIoC.Resolve<IUser>().Lista();
        }
     }
}
