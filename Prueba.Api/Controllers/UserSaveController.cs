﻿using System.Web.Http;
using Autofac;
using Prueba.Management.Interfaces;

namespace Prueba.Api.Controllers
{
    public class UserSaveController : ApiController
    {
        public bool Get(int id, string name, string lastName, string address)
        {
          
            var contenedorIoC = ConfigurationIoC.GeneradorContainer();
            return contenedorIoC.Resolve<IUser>().Save(id, name, lastName, address);
        }
    }
}
