﻿namespace Prueba.Management
{
    using Prueba.Entity;
    using Prueba.Management.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Data.SQLite;

    public class UserManagement :IUser
    {
        /// <summary>
        /// Caller Name
        /// </summary>
        private const string CallerName = "Prueba.Management.UserManagement";

        public bool Delete (int Id)
        {
            string callerNameFuncion = $"{CallerName}.Delete";
            SQLiteConnection ObjCon = new SQLiteConnection();
            SQLiteCommand ObjCommand = new SQLiteCommand();

            try
            {
                ObjCon = new SQLiteConnection("Data Source=" + Cadena_BD());
                ObjCon.Open();
                ObjCommand = ObjCon.CreateCommand();
                string query = string.Empty;
                query = "Delete From User where id="+Id.ToString();
                ObjCommand.CommandText = query;
                ObjCommand.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                if (ObjCon != null)
                {
                    ObjCon.Close();
                }
            }
            return true;
        }
        public bool Save (int id, string name, string lastName, string address )
        {
            string callerNameFuncion = $"{CallerName}.Save";
            SQLiteConnection ObjCon = new SQLiteConnection();
            SQLiteCommand ObjCommand = new SQLiteCommand();
            try
            {
                ObjCon = new SQLiteConnection("Data Source=" + Cadena_BD());
                ObjCon.Open();
                ObjCommand = ObjCon.CreateCommand();
                string query = string.Empty;
                if (id == 0)
                {
                    query = "insert into User(Name, LastName, Address, CreateDate) values('" + name + "','" + lastName + "','" + address + "','" + DateTime.Now.ToShortDateString()+" "+ DateTime.Now.ToShortTimeString()+ "')";
                }
                else
                {
                    query = "Update User set Name='" + name + "', LastName='" + lastName + "', Address='" + address + "', UpdateDate ='" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "' where Id="+id.ToString();
                }

                ObjCommand.CommandText = query;
                ObjCommand.ExecuteNonQuery();
              
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                if (ObjCon != null)
                {
                    ObjCon.Close();
                }
            }
            return true;
        }
        public List<User> Lista()
        {
            List<User> Lista = new List<User>();
            string callerNameFuncion = $"{CallerName}.Lista";
            SQLiteConnection ObjCon = new SQLiteConnection();
            SQLiteCommand ObjCommand = new SQLiteCommand();
            SQLiteDataReader ObjReader;
            try
            {
                ObjCon = new SQLiteConnection("Data Source="+ Cadena_BD());
                ObjCon.Open();
                ObjCommand = ObjCon.CreateCommand();
                ObjCommand.CommandText = "select Id,Name,LastName,Address,CreateDate,UpdateDate from User";
                ObjReader = ObjCommand.ExecuteReader();
               while (ObjReader.Read())
                {
                    Lista.Add(new User()
                    {
                        Id = Convert.ToInt32(ObjReader["Id"].ToString()),
                        Name = ObjReader["Name"].ToString(),
                        LastName = ObjReader["LastName"].ToString(),
                        Address = ObjReader["Address"].ToString(),
                        CreateDate = ObjReader["CreateDate"].ToString(),
                        UpdateDate = ObjReader["UpdateDate"].ToString()
                    });
                }
            }
            finally
            {
                if (ObjCon != null)
                {
                    ObjCon.Close();
                }
            }
            return Lista;
        }
        private string Cadena_BD()
        {
           string reg= System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\x_Prueba.db";
           return reg.Substring(6);
        }
    }
}
