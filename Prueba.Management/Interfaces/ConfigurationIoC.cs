﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;


namespace Prueba.Management.Interfaces
{
    public static class ConfigurationIoC
    {
        public static IContainer GeneradorContainer()
        {
            var buider = new ContainerBuilder();
            buider.RegisterType<UserManagement>().As<IUser>();
            return buider.Build();
        }
    }
}
