﻿using Prueba.Entity;
using System.Collections.Generic;

namespace Prueba.Management.Interfaces
{
    public interface IUser
    {
        List<User> Lista();
        bool Delete(int Id);
        bool Save(int id, string name, string lastName, string address);

    }
}
